<?php 
include ('base.php'); 
include_once('comprobar_session.php');
echo $cabecera;
$id_usuario_loggeado = $_SESSION['id_usuario'];
?>

<?php
	try{
		echo '<br/><h2>EDITAR CONTACTO</h2>';
		$conn = new PDO('sqlite:agenda.db');
		
		$listado='SELECT * FROM agenda WHERE id_usuario="'.$id_usuario_loggeado.'" ORDER BY nombre';
		$resultado=$conn->query($listado);
		
		echo '<form name="editar" method="POST" action="asegurarEditar.php">';
		echo '<table class="tabladatos">';
			echo "<tr>";
				echo "<th></th>";
				echo "<th>Id</th>";
				echo "<th>Nombre</th>";
				echo "<th>Apellidos</th>";
				echo "<th>Telefono</th>";
				echo "<th>Email</th>";
			echo "</tr>";
			foreach($resultado as $ag){
				echo "<tr>";
					echo '<td><input type="radio" name="edit" value='.$ag["id"].'</td>';
					echo "<td>",$ag["id"],"</td>";
					echo "<td>",$ag["nombre"],"</td>";
					echo "<td>",$ag["apellidos"],"</td>";
					echo "<td>",$ag["telefono"],"</td>";
					echo "<td>",$ag["email"],"</td>";
				echo "</tr>";
			}
			echo '<tr><td colspan="6" align="right"><input type="submit" value="Editar Registro"/></td></tr>';
		echo "</table>";
		echo "</form>";
		
	}catch(PDOException $e){
		echo $e->getMessage();
	}
	//cierra conexion
	$conn=null;
?>

<?php 
	echo $pie;
?>